import React from 'react';
import './App.css';
class App extends React.Component {
    render() {
        return (
            <section className='App'>
                <h1>Hello From JSX</h1>
            </section>
        )
    }
}
export default App;
