import React from "react";
import '../App.css'
import './saludo.css'

const Subtitle = (prop)=> {
        return(
            <h1 className='child'>Hello, world {prop.elemento}</h1>
        )
}

class Saludo extends React.Component{
    state = {
        bien: false
    }

    toggle = () =>{
       let bien = this.state.bien
        if (bien){
            this.setState({bien:false})
        }else {
            this.setState({bien:true})
        }
    }

    saludos = () => {
        return {
           normal: 'Saludos cordiales desde esta App de React',
            marginal:'qlq manaure'
        }
    }

    //template
    render() {
            if (this.state.bien){
                return(
                    <section>
                        <button className='button' onClick={this.toggle}>Cambiar</button>
                        <h2>
                            {this.saludos().normal}
                        </h2>
                        <Subtitle elemento={this.saludos().normal}/>
                    </section>
                )
            }
            else {
                return(
                    <section>
                        <button className='button'  onClick={this.toggle}>Cambiar</button>
                        <h2>
                            {this.saludos().marginal}
                        </h2>
                        <Subtitle elemento={this.saludos().marginal}/>
                    </section>
                )
            }
    }
}

export default Saludo