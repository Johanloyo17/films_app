import React from 'react'
import './card.css'

class Card extends React.Component {
    //data
    state = {
        showMessage : true
    }

    //methods
    cambio = ()=>{
        this.setState({showMessage: !this.state.showMessage})
        console.log(Card)
    }
    //template
    render() {
        if (this.state.showMessage){
            return (
                <section className='card'>
                    <h4>{this.props.title}</h4>
                    <span>{this.props.subTitle}</span>
                    <hr/>
                    <button onClick={this.cambio} className='button'>
                        cambiazo!
                    </button>
                </section>
            )
        }else {
            return (
                <div>
                    <h2>¡No hay contenido!</h2>
                    <button onClick={this.cambio}  className='button'>
                        cambiazo!
                    </button>
                </div>
            )
        }


    }
}

export default Card